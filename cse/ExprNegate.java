package cse;

public class ExprNegate extends ExprUnaryOperator {
	private Expr expr;
	
	public ExprNegate(Expr expr){
		super(expr, "-", 40);
		this.expr = expr;
	}

	@Override
	public Expr translate(int colOffset, int rowOffset) {
		return new ExprNegate(expr);
	}

	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		double ret = expr.evaluate(ssModel).toReal();
		ret = ret * -1.0;
		return new ExprReal(ret);
	}
}