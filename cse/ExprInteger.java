package cse;

public class ExprInteger extends ExprPrimitive{
	private int i;

	public ExprInteger(int i) {
		super();
		this.i = i;		
	}

	@Override
	public Expr translate(int colOffset, int rowOffset) {		
		return this;
	}

	public double toReal(){
		return i;
	}
	public String toString(){
		return i + "";
	}
	public int toInteger(){
		return i;
	}
	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		return this;
	}
}