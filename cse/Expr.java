package cse;

public abstract class Expr {
	private int precedence;
	public Expr(int precedence) {
		this.precedence = precedence;
	}

	public abstract Expr evaluate(SpreadsheetModel ssModel);
	public abstract Expr translate(int colOffset, int rowOffset);
	public abstract String toSource();
	
	public int toInteger() throws UnsupportedOperationException{
		return 0;		
	}
	public double toReal() throws UnsupportedOperationException{
		return 0;		
	}
	@Override
	public String toString() throws UnsupportedOperationException{
		return "";		
	}

	public int getPrecedence() {
		return precedence;
	}
}
