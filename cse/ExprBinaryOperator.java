package cse;

public abstract class ExprBinaryOperator extends Expr{
	private Expr lExpr;
	private Expr rExpr;
	private String operator;

	public ExprBinaryOperator(Expr lExpr, Expr rExpr, String operator, int precedence) {
		super(precedence);
		this.lExpr = lExpr;
		this.rExpr = rExpr;
		this.operator = operator;
	}
	
	public String toSource(){
		String ret = lExpr.toSource();
		if(this.getPrecedence() > lExpr.getPrecedence()){
			ret = "(" + ret + ")";			
		}
		ret = ret + " " + operator.toString() + " ";
		
		if(this.getPrecedence() > rExpr.getPrecedence()){
			ret = ret + "(" + rExpr.toSource() + ")";			
		}else{
			ret = ret + rExpr.toSource();			
		}
		
		return ret;
	}
}