Everytime you sit down to work, follow these steps:

  1. Sync if your instructor has changed files.
  2. git pull
  3. Make your changes.
  4. git add any new files.
  5. git commit -a
  6. git push
  7. Verify on Bitbucket.
