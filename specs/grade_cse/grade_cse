#!/usr/bin/env zsh

# ---------------------------------------------------------------------------- 

function check_tokens() {
  file=$1
  shift
  for i in $*; do
    grep -- $i $file >/dev/null
    if [[ $? -ne 0 ]]; then
      echo "Yikes! Expected $i in $file not found." >& 2
      exit 1
    fi
  done
}

# ---------------------------------------------------------------------------- 

iskey=0
isgrader=0
while getopts kg opt; do
  case $opt in
    (k)
      iskey=1
      ;;
    (g)
      isgrader=1
      ;;
  esac
done

grader_dir=${0:A:h}
srcdir=$(pwd)
basedir=${srcdir:t}
tmp="${srcdir}/.tmp"
name="cse"

antlr=$grader_dir/antlr-4.5-complete.jar

# Assert that we're in the right directory.
if [[ $basedir != $name ]]; then
  echo "Oh no! The grader must be run from the $name directory." >&2
  exit 1
fi

# ---------------------------------------------------------------------------- 

# Assert that all required files exist.
expected_files=(CommaSeparatedExpression.g Expr.java ExprAdd.java ExprAddress.java ExprBinaryOperator.java ExprDivide.java ExprFunctionCall.java ExprInteger.java ExprMax.java ExprMean.java ExprMin.java ExprMultiply.java ExprNegate.java ExprPower.java ExprPrimitive.java ExprReal.java ExprRemainder.java ExprString.java ExprSubtract.java ExprSum.java ExprUnaryOperator.java Interpreter.java Address.java)
for i in $expected_files; do
  if [[ ! -f $i ]]; then
    echo "Whoops! Expected file $i not found." >&2
    exit 1
  fi
done

# ---------------------------------------------------------------------------- 

if [[ ! -f $tmp/junit.jar ]]; then
  echo "Downloading JUnit..."
  (cd $tmp && wget -O junit.jar 'http://search.maven.org/remotecontent?filepath=junit/junit/4.12/junit-4.12.jar')
fi

if [[ ! -f $tmp/hamcrest.jar ]]; then
  echo "Downloading Hamcrest..."
  (cd $tmp && wget -O hamcrest.jar 'http://search.maven.org/remotecontent?filepath=org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar')
fi

BUILDPATH=$grader_dir/antlr-4.5-complete.jar:$tmp/junit.jar:$tmp/hamcrest.jar:.:$grader_dir/speccheck_cse.jar

# Compile
java -jar $antlr -package cse -o $tmp/cse CommaSeparatedExpression.g 
javac -cp $antlr -d $tmp $tmp/cse/CommaSeparatedExpression*.java
javac -cp $BUILDPATH:$tmp -d $tmp Expr*.java Address.java Interpreter.java

gotlater=0
java -cp $BUILDPATH:$tmp cse.speccheck.SpecChecker -l
specchecker_status=$?
if [[ $specchecker_status -ne 0 ]]; then
  if [[ $specchecker_status -eq 10 ]]; then
    gotlater=1
  fi
  hay_trouble=1
fi

# ----------------------------------------------------------------------------

if [[ $gotlater -ne 1 && $hay_trouble -ne 0 ]]; then
  exit 1
elif [[ $hay_trouble -eq 1 ]]; then
  exit 2
elif [[ $iskey -eq 1 ]]; then
  exit 0
fi

# Assert that a commit and push has happened or is about to happen.
if [[ $isgrader -eq 0 ]]; then
  echo
  echo "Have you added any unadded files, committed, and pushed to Bitbucket?"
  echo -n "Or are you about to? Run \"git status\" if you don't know. y or [n]? "
  read answer
  if [[ "$answer" != "y" ]]; then
    exit 2
  fi
fi

exit 0
