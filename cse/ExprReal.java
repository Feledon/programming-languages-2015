package cse;

public class ExprReal extends ExprPrimitive{
	private double d;

	public ExprReal(double d) {
		super();
		this.d = d;
	}

	@Override
	public Expr translate(int colOffset, int rowOffset) {
		return new ExprReal(this.toReal());
	}

	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		return new ExprReal(this.toReal());
	}
	public String toString(){
		return d + "";
	}
	public int toInteger(){
		return (int) d;
	}
	public double toReal(){
		return d;
	}
}