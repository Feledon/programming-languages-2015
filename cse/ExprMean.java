package cse;

public class ExprMean extends ExprFunctionCall {
	private Address start;
	private Address finish;

	public ExprMean(Address from, Address to) {
		super(from, to, "Mean");
		this.start = from;
		this.finish = to;
	}

	@Override
	public Expr translate(int colOffset, int rowOffset) {
		Address newFrom = new Address(this.start.translate(colOffset, rowOffset).toString());
		Address newTo = new Address(this.finish.translate(colOffset, rowOffset).toString());
		
		ExprSum newExpr = new ExprSum(newFrom, newTo);
		return newExpr;
	}

	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		double count = 0;
		double current = 0;
		for (int j = start.getColumn(); j < finish.getColumn() + 1; ++j){
			for(int i = start.getRow(); i < finish.getRow() + 1; ++i){
				count ++;
				current += ssModel.evaluate(j, i).evaluate(ssModel).toReal();				
			}
		}
		double mean = current/count;
		return new ExprReal(mean);
	}
}
