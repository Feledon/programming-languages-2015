#include "Music.h"
#include <iostream>
#include <cmath>

namespace music{
	int GetHalfStepID(const std::string note){		
		char base = note[0];//Get the starting value(C, D, E, F, G, A, B)
		int retValue = 0;

		//Check to see our starting point
		if(base == 'C'){			
			retValue = 0;
		}else if(base =='D'){
			retValue = 2;
		}else if(base =='E'){
			retValue = 4;
		}else if(base =='F'){
			retValue = 5;
		}else if(base =='G'){
			retValue = 7;
		}else if(base =='A'){
			retValue = 9;
		}else if(base =='B'){
			retValue = 11;
		}else{
			retValue = -1;
		}
		
		//Check if we are flat or sharp, this will shift us up or down in value
		if(note[1] == '-'){//flat
			--retValue;
		}else if(note[1] == '+'){//sharp 
			++retValue;
		}

		//Build our octave 		
		char oct;
		if(note.size() > 2){
			oct = note[2];			
		}else{
			oct = note[1];
		}
		
		int octave = oct - '0';
		octave = 12 * octave;
				
		return retValue + octave;
	}
	
	float GetFrequency(const std::string note){
		//frequency of A4 = 440
		//long double a4frequency = 440.0;
		//magic number = 2^1/12
		float div =  (1.0/12.0);
		float magicNumber = pow(2.0f, div);
		//distance of X from A4 = halfstep ID of X - halfstep ID of A4
		float dist = ((float)GetHalfStepID(note) - (float)GetHalfStepID("A4"));
		//frequency of X = frequency of A4 � magic number^distance of X from A4
		std::cout << dist << "=" << GetHalfStepID(note)<<"-"<< GetHalfStepID("A4") << ":" << magicNumber<< ":"<< (pow((float)magicNumber, (float)dist)) << std::endl; 
		
		return (float)(440.0f * ((float)pow((float)magicNumber, (float)dist)));
	}
}