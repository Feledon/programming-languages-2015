package cse;

public class ExprPower extends ExprBinaryOperator {
	private Expr lExpr;
	private Expr rExpr;
	
	public ExprPower(Expr lExpr, Expr rExpr) {
		super(lExpr, rExpr, "^", 30);
		this.lExpr = lExpr;
		this.rExpr = rExpr;
	}

	@Override
	public Expr translate(int colOffset, int rowOffset) {		
		ExprPower newExpr = new ExprPower(this.lExpr.translate(colOffset, rowOffset), this.rExpr.translate(colOffset, rowOffset));
		return newExpr;
	}

	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		double total = Math.pow(lExpr.evaluate(ssModel).toReal(), rExpr.evaluate(ssModel).toReal());
		return new ExprReal(total);
	}
}