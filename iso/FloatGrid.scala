
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.io.FileInputStream
import java.nio.ByteOrder
import java.nio.channels.FileChannel.MapMode

class FloatGrid (path: String){
  val width = path.split('.')(1).toInt
  val height = path.split('.')(2).toInt
  
  val grid = new Array[Float](width * height)
  private var _min = Float.MaxValue
  private var _max = Float.MinValue 
  
	{//Block to hide local stuff, good according to tester
		val file = new File(path)
		val stream = new FileInputStream(file)
		val buffer = stream.getChannel.map(MapMode.READ_ONLY, 0, file.length)    
		buffer.order(ByteOrder.LITTLE_ENDIAN)	
		var i = 0
    
    while(buffer.hasRemaining()){
      val curr = buffer.getFloat
			grid(i) = curr
      if(curr < _min){
        _min = curr
      }
      if(curr > _max){
        _max = curr
      }
			i = i + 1
		}  
    buffer.clear
    stream.close()
	}
  
  def min = _min
  def max = _max 

  def apply(c: Int, r: Int): Float = {
    grid(c + r * width).toFloat
  }

  def each (body: (Int, Int, Float) => Unit) = {
    for (c <- 0 until width; r <- 0 until height) {
		  body(c, r, this.apply(c, r)) 
	  }
  }

  def toImage: BufferedImage = {
    val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
    for (c <- 0 until width; r <- 0 until height) {
      var colo = (((apply(c, r) - min)/(max - min)) * 255).toInt
      image.setRGB(c, r, new Color(colo, colo, colo).getRGB)    
    }
    image
  }
  
  def mapOverCells[T](body: (Int, Int, Float, Float, Float, Float) => T): List[T] = {
    var x = for (c <- (0 until width - 1).par; r <- (0 until height - 1).par) yield {
        body(c, r, this.apply(c, r), this.apply(c + 1, r),this.apply(c, r + 1),this.apply(c + 1, r + 1))
    }    
   x.toList
  }  
}//End Class