package cse;

public class ExprAdd extends ExprBinaryOperator{
	private Expr lExpr;
	private Expr rExpr;
		
	public ExprAdd(Expr lExpr, Expr rExpr) {
		super(lExpr, rExpr, "+", 10);
		this.lExpr = lExpr;
		this.rExpr = rExpr;
	}

	@Override
	public Expr translate(int colOffset, int rowOffset) {
		ExprAdd newExpr = new ExprAdd(this.lExpr.translate(colOffset, rowOffset), this.rExpr.translate(colOffset, rowOffset));
		return newExpr;
	}

	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		double total = lExpr.evaluate(ssModel).toReal() + rExpr.evaluate(ssModel).toReal();
		return new ExprReal(total);
	}
}