package cse;

public class ExprString extends ExprPrimitive {
	private String expr;
	
	public ExprString(String expr){
		super();
		this.expr = expr;
	}
	
	@Override
	public Expr translate(int colOffset, int rowOffset) {
		return this;
	}
	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		return this;
	}
	public double toReal(){
		return Double.parseDouble(expr);
	}
	public int toInteger(){
		return Integer.parseInt(expr);
	}
	public String toString(){
		return expr;
	}
}