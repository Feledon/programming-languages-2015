package cse;

public abstract class ExprFunctionCall extends Expr {
	private Address from;
	private Address to;
	private String funcName;

	public ExprFunctionCall(Address from, Address to, String funcName) {
		super(50);
		this.from = from;
		this.to = to;
		this.funcName = funcName;
	}

	public String toSource(){
		return funcName + "(" + from.toString() + ":" + to.toString() + ")";
	}

}
