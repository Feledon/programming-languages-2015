package cse;

public abstract class ExprPrimitive  extends Expr {
	
	public ExprPrimitive() {
		super(60);		
	}

	@Override
	public String toSource(){
		return  this.toString();
	}
}