#include <gtest/gtest.h>

#include "Samples.h"

TEST(Samples, CtorIntFloats) {
  Samples s(1, new float[1]);
  ASSERT_EQ(1, s.GetLength());
}

TEST(Samples, CtorCopy) {
  float *data = new float[5];
  data[0] = 10;
  data[1] = 12;
  data[2] = 20;
  data[3] = -3;
  data[4] = -5;

  Samples *s1 = new Samples(5, data);
  Samples s2(*s1);

  ASSERT_EQ(5, s1->GetLength());
  ASSERT_EQ(5, s2.GetLength());
 
  ASSERT_NEAR(10, (*s1)[0], 1.0e-3f);
  ASSERT_NEAR(12, (*s1)[1], 1.0e-3f);
  ASSERT_NEAR(20, (*s1)[2], 1.0e-3f);
  ASSERT_NEAR(-3, (*s1)[3], 1.0e-3f);
  ASSERT_NEAR(-5, (*s1)[4], 1.0e-3f);

  // Twiddle with s1 to make sure its independent of s2.
  (*s1)[0] = 57;
  (*s1)[1] = 58;
  (*s1)[2] = 59;
  (*s1)[3] = 60;
  (*s1)[4] = 61;

  ASSERT_NEAR(10, s2[0], 1.0e-3f);
  ASSERT_NEAR(12, s2[1], 1.0e-3f);
  ASSERT_NEAR(20, s2[2], 1.0e-3f);
  ASSERT_NEAR(-3, s2[3], 1.0e-3f);
  ASSERT_NEAR(-5, s2[4], 1.0e-3f);
}

TEST(Samples, SubscriptRead) {
  float *data = new float[3];
  data[0] = 10;
  data[1] = 12;
  data[2] = 20;
  const Samples s(3, data);
  ASSERT_NEAR(10, s[0], 1.0e-3f);
  ASSERT_NEAR(12, s[1], 1.0e-3f);
  ASSERT_NEAR(20, s[2], 1.0e-3f);
}

TEST(Samples, SubscriptWrite) {
  float *data = new float[4];
  data[0] = 10;
  data[1] = 12;
  data[2] = 20;
  data[3] = 100;
  Samples s(4, data);
  ASSERT_NEAR(10, s[0], 1.0e-3f);
  ASSERT_NEAR(12, s[1], 1.0e-3f);
  ASSERT_NEAR(20, s[2], 1.0e-3f);
  ASSERT_NEAR(100, s[3], 1.0e-3f);
  for (int i = 0; i < 4; ++i) {
    s[i] = 1000 + i;
  }
  ASSERT_NEAR(1000, s[0], 1.0e-3f);
  ASSERT_NEAR(1001, s[1], 1.0e-3f);
  ASSERT_NEAR(1002, s[2], 1.0e-3f);
  ASSERT_NEAR(1003, s[3], 1.0e-3f);
}

TEST(Samples, Assignment) {
  float *data = new float[4];
  data[0] = -50;
  data[1] = -10;
  data[2] = 40;
  data[3] = 1000;
  Samples s1(4, data);

  Samples s2(440.0f, 4);

  s2 = s1;
  ASSERT_EQ(4, s2.GetLength());
  ASSERT_NEAR(-50, s2[0], 1.0e-3f);
  ASSERT_NEAR(-10, s2[1], 1.0e-3f);
  ASSERT_NEAR(40, s2[2], 1.0e-3f);
  ASSERT_NEAR(1000, s2[3], 1.0e-3f);
}

TEST(Samples, SelfAssignment) {
  float *data = new float[4];
  data[0] = -50;
  data[1] = -10;
  data[2] = 40;
  data[3] = 1000;
  Samples s1(4, data);

  s1 = s1;
  ASSERT_EQ(4, s1.GetLength());
  ASSERT_NEAR(-50, s1[0], 1.0e-3f);
  ASSERT_NEAR(-10, s1[1], 1.0e-3f);
  ASSERT_NEAR(40, s1[2], 1.0e-3f);
  ASSERT_NEAR(1000, s1[3], 1.0e-3f);
}

TEST(Samples, TimesEquals) {
  float *data = new float[51];
  for (int i = -25; i <= 25; ++i) {
    data[i + 25] = i;
  }
  Samples s(51, data);
  s *= 3;
  for (int i = -25; i <= 25; ++i) {
    ASSERT_NEAR(i * 3, s[i + 25], 1.0e-3f);
  }
}

TEST(Samples, OrEquals) {
  float *data1 = new float[100];
  float *data2 = new float[100];
  for (int i = 0; i < 100; ++i) {
    data1[i] = 100 + i;
    data2[i] = -100 - i;
  }
  Samples s1(100, data1);
  Samples s2(100, data2);

  s1 |= s2;

  for (int i = 0; i < 100; ++i) {
    ASSERT_NEAR(-100 - i, s2[i], 1.0e-3f);
    ASSERT_NEAR(-100 - i + 100 + i, s1[i], 1.0e-3f);
  }
}

TEST(Samples, PlusEquals) {
  float *data1 = new float[100];
  float *data2 = new float[30];
  for (int i = 0; i < 100; ++i) {
    data1[i] = 100 + i;
  }
  for (int i = 0; i < 30; ++i) {
    data2[i] = 10 * i;
  }
  Samples s1(100, data1);
  Samples s2(30, data2);

  s1 += s2;

  ASSERT_EQ(130, s1.GetLength());

  for (int i = 0; i < 100; ++i) {
    ASSERT_NEAR(100 + i, s1[i], 1.0e-3f);
  }

  for (int i = 100; i < 130; ++i) {
    ASSERT_NEAR(10 * (i - 100), s1[i], 1.0e-3f);
  }

  for (int i = 0; i < 30; ++i) {
    ASSERT_NEAR(10 * i, s2[i], 1.0e-3f);
  }
}

TEST(Samples, Times) {
  float *data = new float[512];
  for (int i = 0; i < 512; ++i) {
    data[i] = i + 200;
  }
  Samples s1(512, data);

  Samples s2 = s1 * 4;
  ASSERT_EQ(512, s2.GetLength());
  for (int i = 0; i < 512; ++i) {
    ASSERT_NEAR(i + 200, s1[i], 1.0e-3f);
    ASSERT_NEAR((i + 200) * 4, s2[i], 1.0e-3f);
  }
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
