package cse;

public class ExprAddress extends ExprPrimitive{

	private Address addr;
	public ExprAddress(Address addr){
		this.addr = addr;
	}
	
	@Override
	public Expr translate(int colOffset, int rowOffset) {
		return new ExprAddress(addr.translate(colOffset, rowOffset));
	}
		
	 public String toString(){
		 return addr.toString();
	 }

	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		return ssModel.evaluate(addr.getColumn(), addr.getRow());
	}
}
