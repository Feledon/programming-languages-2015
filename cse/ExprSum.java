package cse;

public class ExprSum extends ExprFunctionCall {
	private Address start;
	private Address finish;
	
	public ExprSum(Address from, Address to) {
		super(from, to, "sum");
		this.start = from;
		this.finish = to;
	}

	@Override
	public Expr translate(int colOffset, int rowOffset) {
		Address newFrom = new Address(this.start.translate(colOffset, rowOffset).toString());
		Address newTo = new Address(this.finish.translate(colOffset, rowOffset).toString());
		
		ExprSum newExpr = new ExprSum(newFrom, newTo);
		return newExpr;
	}

	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		double sum = 0;
		for (int j = start.getColumn(); j < finish.getColumn() + 1; ++j){
			for(int i = start.getRow(); i < finish.getRow() + 1; ++i){
				sum += ssModel.evaluate(j, i).evaluate(ssModel).toReal();				
			}
		}
		return new ExprReal(sum);
	}
}
