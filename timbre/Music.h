#ifndef IMAGE_H
#define IMAGE_H
#include <string>
namespace music{
	int GetHalfStepID(const std::string note);
	float GetFrequency(const std::string note);
}
#endif