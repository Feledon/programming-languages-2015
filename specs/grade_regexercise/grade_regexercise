#!/usr/bin/env zsh

iskey=0
isgrader=0
while getopts kg opt; do
  case $opt in
    (k)
      iskey=1
      ;;
    (g)
      isgrader=1
      ;;
  esac
done

graderdir=${0:A:h}
srcdir=$(pwd)
basedir=${srcdir:t}
tmp="${srcdir}/.tmp"
name="regexercise"

# Assert that we're in the right directory.
if [[ $basedir != $name ]]; then
  echo "Oh no! The grader must be run from the $name directory." >&2
  exit 1
fi

# ---------------------------------------------------------------------------- 

hay_trouble=0
function test_argv0_cases_named_expected() {
  theirs=$1
  problem=${theirs:t}
  shift

  # Assert file exists.
  if [[ ! -f $theirs ]]; then
    echo "Whoops! Expected file $theirs not found." >&2
    hay_trouble=1
    return 1
  fi

  while [[ $# -gt 0 ]]; do
    test_case=$1
    expected_file=$2
    shift 2

    if [[ -f "${test_case}.params" ]]; then
      params=$(cat ${test_case}.params)
      params=${params/_/$test_case}
    elif [[ $test_case =~ ".*-params-.*" ]]; then
      params=${test_case:t}
      params=${params/*-params-/}
      params=${params/-/ }
      params=${params/_/$test_case}
    else
      params=$test_case
    fi

    actual_file=$tmp/${problem}-${test_case:t:r}.actual
    if [[ $isgrader -eq 1 ]]; then
      forn 20 ruby $theirs ${=params} > $actual_file
      actual_exit_status=$?
      if [[ $? -gt 128 ]]; then
        echo "Blast it! Command \"ruby $problem ${=params}\" timed out."
      fi
    else
      ruby $theirs ${=params} > $actual_file
      actual_exit_status=$?
    fi
    
    # If we're a key, then use these results as the expected ones and skip to
    # the next test case.
    if [[ $iskey -eq 1 ]]; then
      \cp -f $actual_file $expected_file
      echo $actual_exit_status > ${expected_file}.exit_status
      continue
    fi

    expected_exit_status=$(cat ${expected_file}.exit_status)

    if [[ $actual_exit_status -ne $expected_exit_status ]]; then
      hay_trouble=1
      echo "Ay yay yay. Expected exit status $expected_exit_status on \"ruby $problem ${=params}\"."
      echo -n "You exited with $actual_exit_status. "
      if [[ $isgrader -eq 0 ]]; then
        echo -n "Hit Enter to continue..."
        read answer
      fi
    fi

    diff $expected_file $actual_file >/dev/null
    if [[ $? -ne 0 ]]; then
      hay_trouble=1
      diff $expected_file $actual_file | grep "No newline at end of file" >/dev/null
      if [[ $? -eq 0 ]]; then
        echo "Ack! Output from running \"ruby $problem ${=params}\" doesn't end in a newline." >&2
      else
        echo "Ughh. Differences were detected when running \"ruby $problem ${=params}\"."
        if [[ $isgrader -eq 0 ]]; then
          echo -n "Run vimdiff? (Hit :qa to quit.) [y] or n: "
          read answer
          if [[ "$answer" != "n" ]]; then
            vimdiff -b -R $expected_file $actual_file
          fi  
        fi
      fi
    fi
  done
}

# ---------------------------------------------------------------------------- 

function test_argv0_cases() {
  theirs=$1
  problem=${theirs:t}
  shift
  
  params=()
  while [[ $# -gt 0 ]]; do
    argv0=$1
    params+=($argv0)
    params+=($graderdir/tests/$problem/expecteds/${argv0:t:r}.expected)
    shift
  done

  test_argv0_cases_named_expected $theirs $params
}

# ---------------------------------------------------------------------------- 

gotlater=0

test_cases=($graderdir/tests/wrap/ins/*)
test_argv0_cases wrap $test_cases

test_cases=($graderdir/tests/classify/ins/*)
test_argv0_cases classify $test_cases

# Later-week submission.
if [[ $hay_trouble -eq 0 ]]; then
  gotlater=1
fi

test_cases=($graderdir/tests/imsort/ins/*)
test_argv0_cases imsort $test_cases

test_cases=($graderdir/tests/mesozoicize/ins/*)
test_argv0_cases mesozoicize $test_cases

# Since the headers are defined using relative paths, we'll run this one in
# the ins directory.
test_cases=($graderdir/tests/preprocess/ins/*-params-*)
(cd $graderdir/tests/preprocess/ins && test_argv0_cases ${srcdir}/preprocess $test_cases)

test_cases=($graderdir/tests/grepp/ins/*.in)
test_argv0_cases grepp $test_cases

test_cases=($graderdir/tests/idread/ins/*)
test_argv0_cases idread $test_cases

test_cases=($graderdir/tests/metricate/ins/*)
test_argv0_cases metricate $test_cases

test_cases=($graderdir/tests/geocode/ins/*)
test_argv0_cases geocode $test_cases

test_cases=($graderdir/tests/deadlinks/ins/*)
test_argv0_cases deadlinks $test_cases

# ---------------------------------------------------------------------------- 

if [[ $gotlater -ne 1 ]]; then
  exit 1
elif [[ $hay_trouble -eq 1 ]]; then
  exit 2
elif [[ $iskey -eq 1 ]]; then
  exit 0
fi

# Assert that a commit and push has happened or is about to happen.
if [[ $isgrader -eq 0 ]]; then
  echo -n "Did you use zero conditional statements in mesozoicize? y or [n]? "
  read answer
  if [[ "$answer" != "y" ]]; then
    exit 2
  fi
  echo

  echo "Have you added any unadded files, committed, and pushed to Bitbucket?"
  echo -n "Or are you about to? Run \"git status\" if you don't know. y or [n]? "
  read answer
  if [[ "$answer" != "y" ]]; then
    exit 2
  fi
fi

exit 0
