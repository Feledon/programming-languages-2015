package cse;

public abstract class ExprUnaryOperator extends Expr{
	private Expr subExpr;
	private String operator;
	
	public ExprUnaryOperator(Expr subExpr, String operator, int precedence){
		super(precedence);
		this.subExpr = subExpr;
		this.operator = operator;
	}

	@Override
	public String toSource(){
		if(this.subExpr.getPrecedence() < this.getPrecedence()){
			return  operator + "(" + this.subExpr.toSource() + ")";	
		}else{
			return  operator + this.subExpr.toSource();	
		}
	}
}