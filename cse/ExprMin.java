package cse;

public class ExprMin extends ExprFunctionCall {
	private Address start;
	private Address finish;
	

	public ExprMin(Address from, Address to) {
		super(from, to, "Min");
		this.start = from;
		this.finish = to;
	}

	@Override
	public Expr translate(int colOffset, int rowOffset) {
		Address newFrom = new Address(this.start.translate(colOffset, rowOffset).toString());
		Address newTo = new Address(this.finish.translate(colOffset, rowOffset).toString());
		
		ExprSum newExpr = new ExprSum(newFrom, newTo);
		return newExpr;
	}

	@Override
	public Expr evaluate(SpreadsheetModel ssModel) {
		double lowest = Integer.MAX_VALUE;
		double current = 0;
		for (int j = start.getColumn(); j < finish.getColumn() + 1; ++j){
			for(int i = start.getRow(); i < finish.getRow() + 1; ++i){
				current = ssModel.evaluate(j, i).evaluate(ssModel).toReal();	
				if(current < lowest){
					lowest = current;
				}
							
			}
		}
		return new ExprReal(lowest);
	}

}
