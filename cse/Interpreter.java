package cse;

import java.util.Stack;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;


public class Interpreter extends CommaSeparatedExpressionBaseListener {
	private static Stack<Expr> operands;

	public Interpreter() {
		operands =  new Stack<Expr>();
	}

	public static Expr parseToAST(String expr){
		Interpreter interpreter = new Interpreter();
		ANTLRInputStream ais = new ANTLRInputStream(expr);
		CommaSeparatedExpressionLexer lexer = new CommaSeparatedExpressionLexer(ais);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		CommaSeparatedExpressionParser parser = new CommaSeparatedExpressionParser(tokens);
		ParseTree tree = parser.line();

		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(interpreter, tree);

		return Interpreter.operands.pop();
	}

	@Override 
	public void exitMax(CommaSeparatedExpressionParser.MaxContext ctx) { 
		Address from = new Address(ctx.expr(0).getText());
		Address to = new Address(ctx.expr(1).getText());

		ExprMax newExpr = new ExprMax(from, to);
		operands.push(newExpr);
	}

	@Override 
	public void exitMean(CommaSeparatedExpressionParser.MeanContext ctx) { 
		Address from = new Address(ctx.expr(0).getText());
		Address to = new Address(ctx.expr(1).getText());

		ExprMean newExpr = new ExprMean(from, to);
		operands.push(newExpr);
	}

	@Override 
	public void exitSum(CommaSeparatedExpressionParser.SumContext ctx) { 
		Address from = new Address(ctx.expr(0).getText());
		Address to = new Address(ctx.expr(1).getText());

		ExprSum newExpr = new ExprSum(from, to);
		operands.push(newExpr);
	}

	@Override 
	public void exitMin(CommaSeparatedExpressionParser.MinContext ctx) {
		Address from = new Address(ctx.expr(0).getText());
		Address to = new Address(ctx.expr(1).getText());

		ExprMin newExpr = new ExprMin(from, to);
		operands.push(newExpr);
	}

	@Override 
	public void exitAddress(CommaSeparatedExpressionParser.AddressContext ctx) { 
		String addressString = ctx.IDENTIFIER().getText();
		Address add = new Address(addressString);
		ExprAddress newExpr = new ExprAddress(add);
		operands.push(newExpr); 
	}

	@Override 
	public void exitPower(CommaSeparatedExpressionParser.PowerContext ctx) { 
		Expr b = operands.pop();
		Expr a = operands.pop();
		operands.push(new ExprPower(a, b));
	}

	//Below added
	@Override 
	public void exitMultiply(CommaSeparatedExpressionParser.MultiplyContext ctx) { 
		Expr b = operands.pop();
		Expr a = operands.pop();

		if (ctx.MULTIPLICATIVE_OPERATOR().getText().equals("*")) {
			operands.push(new ExprMultiply(a, b));
		} else if (ctx.MULTIPLICATIVE_OPERATOR().getText().equals("/")) {
			operands.push(new ExprDivide(a, b));
		}else {
			operands.push(new ExprRemainder(a, b));
		}		
	}
	@Override 
	public void exitSubtract(CommaSeparatedExpressionParser.SubtractContext ctx) { 
		Expr b = operands.pop();
		Expr a = operands.pop();
		Expr newExpr = new ExprSubtract(a, b);
		operands.push(newExpr);
	}
	@Override
	public void exitAdd(CommaSeparatedExpressionParser.AddContext ctx){
		Expr b = operands.pop();
		Expr a = operands.pop();
		Expr newExpr = new ExprAdd(a, b);
		operands.push(newExpr);
	}
	@Override
	public void exitInteger(CommaSeparatedExpressionParser.IntegerContext ctx) {
		String digitsAsString = ctx.DIGITS().getText(); 
		int digits = Integer.parseInt(digitsAsString);
		ExprInteger newExpr = new ExprInteger(digits);
		operands.push(newExpr);
	}
	@Override 
	public void exitPositiveReal(CommaSeparatedExpressionParser.PositiveRealContext ctx) { 
		String digitsAsString = ctx.DIGITS().get(0).getText() + ctx.DIGITS().get(1).getText() + ctx.DIGITS().get(2).getText() ; 
		double digits = Double.parseDouble(digitsAsString);
		ExprReal newExpr = new ExprReal(digits);
		operands.push(newExpr);
	}
	@Override 
	public void exitNegativeReal(CommaSeparatedExpressionParser.NegativeRealContext ctx) { 
		String digitsAsString = ctx.DIGITS().get(0).getText() + ctx.DIGITS().get(1).getText() + ctx.DIGITS().get(2).getText() ; 
		double digits = Double.parseDouble(digitsAsString);
		ExprReal firstExpr = new ExprReal(digits);
		ExprNegate newExpr = new ExprNegate(firstExpr);
		operands.push(newExpr);
	}
	@Override 
	public void exitNegativeInteger(CommaSeparatedExpressionParser.NegativeIntegerContext ctx) { 
		String digitsAsString = ctx.DIGITS().getText(); 
		int digits = Integer.parseInt(digitsAsString);
		ExprInteger firstExpr = new ExprInteger(digits);
		ExprNegate newExpr = new ExprNegate(firstExpr);
		operands.push(newExpr);
	}
}