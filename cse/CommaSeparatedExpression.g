grammar CommaSeparatedExpression;

line
  : expr 
  ;

expr
  : MEAN LEFT_PARENTHESIS expr COLON expr RIGHT_PARENTHESIS # Mean
  | MIN LEFT_PARENTHESIS expr COLON expr RIGHT_PARENTHESIS # Min
  | MAX LEFT_PARENTHESIS expr COLON expr RIGHT_PARENTHESIS # Max
  | SUM LEFT_PARENTHESIS expr COLON expr RIGHT_PARENTHESIS # Sum
  | NEGATIVE DIGITS # NegativeInteger
  | DIGITS POINT DIGITS # PositiveReal
  | NEGATIVE DIGITS POINT DIGITS # NegativeReal
  | LEFT_PARENTHESIS expr RIGHT_PARENTHESIS # Grouped
  | expr POWER expr # Power
  | expr MULTIPLICATIVE_OPERATOR expr # Multiply
  | expr ADDITIVE_OPERATOR expr # Add
  | expr SUBTRACTIVE_OPERATOR expr # Subtract
  | IDENTIFIER # Address
  | DIGITS # Integer
  ;
COLON: ':';
SUM: 'sum';
MIN: 'min';
MAX: 'max';
MEAN: 'mean';
POINT: '.';
NEGATIVE: '-';
LEFT_PARENTHESIS: '(';
RIGHT_PARENTHESIS: ')';
POWER: '^';
MULTIPLICATIVE_OPERATOR: [*/%];
ADDITIVE_OPERATOR:  '+';
SUBTRACTIVE_OPERATOR: '- ';
ASSIGNMENT: ':=';
IDENTIFIER: '$'?[A-Z]'$'?[0-9]+;
DIGITS: [0-9]+;

WHITESPACE: [ \n\r\t]+ -> skip;
