package cse;

import java.util.ArrayList;
import java.util.Arrays;

public class Address{
	private boolean rowLock;
	private boolean colLock;
	private int row;
	private int column;
	private ArrayList<String> colList = new ArrayList<String>(Arrays.asList("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"));
	
	public Address(int column, boolean rowLock, int row, boolean colLock){
		this.column = column;
		this.rowLock = rowLock;
		this.colLock= colLock;
		this.row = row;
	}

	public Address(String addr){
		this.rowLock = false;
		this.colLock= false;
		this.column = 0;
		this.row = 0;
	
		//Have to check for leading $ -> $A$5
		if(addr.substring(0,1).equalsIgnoreCase("$")){
			colLock = true;
			addr = addr.substring(1);
		}
		this.column = colList.indexOf(addr.substring(0,1));
		
		if (addr.contains("$")){
			rowLock = true;
			this.row = Integer.parseInt(addr.substring(2));
		}else{		
			this.row = Integer.parseInt(addr.substring(1));
		}
	}

	public int getRow(){
		return this.row;
	}

	public int getColumn(){
		return this.column;
	}

	public Address translate(int columnOffset, int rowOffset){
		int newCol = this.column;
		if(!colLock){
			newCol += columnOffset;
		}
		
		int newRow = this.row;
		if(!rowLock){
			newRow += rowOffset;
		}
		return new Address(newCol, this.rowLock, newRow, this.colLock);
	}

	public String toString(){
		String ret = "";
		if(colLock){
			ret += "$";
		}
		
		ret += colList.get(column);
		
		if(rowLock){
			ret += "$";
		}
		
		ret += row;
		
		return ret;
	}
}
