class LineSegment(val a: Point2, val b: Point2){
  override def toString: String = {
    a.toString + " to " + b.toString()
  }
}