import java.awt.Color
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.io.File
import java.awt.BasicStroke
import java.awt.Graphics2D
import java.awt.geom.Line2D

object HeadlessRenderer {
  def main(args: Array[String]) {
    if (args.length != 3) {
      println("Usage: scala HeadlessRenderer elevation_file.<WIDTH>.<HEIGHT>.bin <isovalue> out.png")
      System.exit(1)
    }

    val grid = new FloatGrid(args(0))
    val iso = args(1).toFloat
    val segments = grid.mapOverCells(Contourer.cellToSegments(iso)).flatten

    val width = 768
    val height = 512
    val image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

    val g2 = image.createGraphics
    val oldTransform = g2.getTransform

    val gridAspect = grid.width / grid.height.toDouble
    val windowAspect = width / height.toDouble
    val scaleFactor = if (windowAspect < gridAspect) {
      width / grid.width.toDouble
    } else {
      height / grid.height.toDouble
    }
    g2.scale(scaleFactor, scaleFactor)
    g2.drawImage(grid.toImage, 0, 0, null)

    g2.setStroke(new BasicStroke((1.0 / scaleFactor).toFloat))

    g2.translate(0.5, 0.5)
    g2.setColor(Color.YELLOW)
    segments.foreach {segment :LineSegment => {
      g2.draw(new Line2D.Float(segment.a.x, segment.a.y, segment.b.x, segment.b.y))
    }}

    g2.setTransform(oldTransform)

    ImageIO.write(image, "png", new File(args(2)))
  }
}
