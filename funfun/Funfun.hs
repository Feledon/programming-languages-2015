module Funfun where

import Data.List
import Data.Function
import Data.Function.Memoize

twoByTwo :: [t]->[(t,t)]
twoByTwo [] = []
twoByTwo [x] = []
twoByTwo (first:second:rest) = (first, second): twoByTwo rest

sumOfSquares ::(Num t)=>[t]->t 
sumOfSquares = sum . map (^(2::Integer))

squareOfSum ::(Num t) => [t] -> t
squareOfSum = (^2) . sum

squmDiff :: Int -> Int
squmDiff x = squareOfSum [1..x] - sumOfSquares [1..x]

mode :: (Ord t)=>[t] -> t
mode = head .  maximumBy (compare `on` length) .  group . sort

indices :: (Eq t) => [t] -> [t] -> [Int]
indices a b = findIndices(==True) (map (\b->a `isPrefixOf` b) (tails b))

flatten2 :: [[t]] -> [t]
flatten2 = concat  

nroutes :: Int -> Int -> Int
nroutes = memoize2 nroutesHelper
 where
  nroutesHelper :: Int -> Int -> Int 
  nroutesHelper x y
   | (x < 0) || (y < 0) = 0
   | (x == 0) && (y == 0) = 1
   | otherwise = nroutes (x - 1) y + nroutes x (y - 1)

type XY = (Double, Double)

translate :: XY -> XY -> XY
translate x y = ((fst x) + (fst y), (snd x) + (snd y))

scale :: XY -> XY -> XY
scale x y = ((fst x) * (fst y), (snd x) * (snd y)) 

rotate :: Double -> XY -> XY
rotate r (x, y)  = ( (x * (cos (r*pi/180))) - (y * (sin (r*pi/180))) ,  (x * (sin (r*pi/180))) + (y * (cos (r*pi/180))) )

transformAll :: (XY -> XY) -> [XY] -> [XY]
transformAll f xys = map f xys

gauntlet :: [(XY -> XY)] -> XY -> XY
gauntlet fs xy = foldl (flip (.)) id fs $ xy 

gauntletAll :: [(XY -> XY)] -> [XY] -> [XY]
gauntletAll fs xys = transformAll(gauntlet fs) xys 
